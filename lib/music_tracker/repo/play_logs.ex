# Copyright 2022 Nicolas Jouanin <nicolas.jouanin@mailo.com>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# SPDX-License-Identifier: Apache-2.0

defmodule MusicTracker.Repo.PlayLogs do
  alias MusicTracker.Repo
  alias MusicTracker.Repo.Model.PlayLog

  @doc """
  Add a new play log entry to an existing channel
  """
  @spec insert_play_log(channel :: MusicTracker.Repo.Model.Channel.t(), attrs :: map()) ::
          {:ok, Playlog.t()} | {:error, Changeset.t()}
  def insert_play_log(channel, attrs \\ %{}) do
    %PlayLog{} |> PlayLog.create_changeset(channel, attrs) |> Repo.insert()
  end

  @doc """
  Get a play log entry from its primary key
  """
  @spec get_by_id(id :: Ecto.UUID.t()) :: PlayLog | nil
  def get_by_id(id) do
    Repo.get(PlayLog, id)
  end

  @doc """
  Get a play log entry from its primary key
  Channel association is pre-loaded
  """
  @spec get_by_id_with_channel(id :: Ecto.UUID.t()) :: PlayLog | nil
  def get_by_id_with_channel(id) do
    Repo.get(PlayLog, id) |> Repo.preload(:channel)
  end

  @doc """
  Update a play log entry analyzed state
  """
  @spec set_analyzed(playlog :: PlayLog.t(), analyzed :: DateTime.t()) ::
          {:ok, PlayLog.t()} | {:error, Changeset.t()}
  def set_analyzed(playlog, date) do
    Ecto.Changeset.change(playlog, analyzed_at: date) |> Repo.update()
  end
end
