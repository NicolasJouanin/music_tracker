# Copyright 2022 Nicolas Jouanin <nicolas.jouanin@mailo.com>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# SPDX-License-Identifier: Apache-2.0

defmodule MusicTracker.Repo.Model.SongPlayHistory do
  use MusicTracker.Schema
  alias MusicTracker.Repo.Model.Song
  alias MusicTracker.Repo.Model.Channel

  @type t :: %__MODULE__{
          start_time: DateTime.t(),
          end_time: DateTime.t(),
          extract_hash: String.t()
        }

  schema "song_play_history" do
    field(:start_time, :utc_datetime_usec)
    field(:end_time, :utc_datetime_usec)
    field(:extract_hash, :string)
    belongs_to(:song, Song)
    belongs_to(:channel, Channel)
    timestamps(updated_at: false)
  end

  def changeset(song_play_history, attrs \\ %{}) do
    song_play_history
    |> cast(attrs, [:start_time, :end_time, :extract_hash])
    |> validate_required([:start_time, :end_time, :extract_hash])
    |> unique_constraint([:hash])
  end

  def create_changeset(song_play_history, song, channel, attrs \\ %{}) do
    song_play_history
    |> change()
    |> put_assoc(:song, song)
    |> put_assoc(:channel, channel)
    |> changeset(attrs)
  end
end
