# Copyright 2022 Nicolas Jouanin <nicolas.jouanin@mailo.com>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# SPDX-License-Identifier: Apache-2.0

defmodule MusicTracker.Repo.Model.Song do
  use MusicTracker.Schema
  alias MusicTracker.Repo.Model.SongAnalyzerId
  alias MusicTracker.Repo.Model.SongArtist
  alias MusicTracker.Repo.Model.MusicalKind
  alias MusicTracker.Repo.Model.SongPlayHistory
  alias MusicTracker.Repo.Model.SongMedia

  @type t :: %__MODULE__{
          title: String.t(),
          album: String.t(),
          label: String.t(),
          year: Integer
        }

  schema "song" do
    field(:title, :string)
    field(:album, :string)
    field(:label, :string)
    field(:year, :integer)
    has_many(:analyzer_ids, SongAnalyzerId)
    has_many(:artists, SongArtist)
    has_many(:musical_kinds, MusicalKind)
    has_many(:play_history, SongPlayHistory)
    has_many(:medias, SongMedia)
    timestamps()
  end

  def changeset(song, attrs \\ %{}) do
    song
    |> cast(attrs, [:title, :album, :label, :year])
    |> validate_required([:title])
  end

  def create_changeset(song, attrs \\ %{}) do
    song
    |> change()
    |> changeset(attrs)
  end
end
