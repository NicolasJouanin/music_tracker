# Copyright 2022 Nicolas Jouanin <nicolas.jouanin@mailo.com>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# SPDX-License-Identifier: Apache-2.0

defmodule MusicTracker.Repo.Model.MusicalKind do
  use MusicTracker.Schema
  alias MusicTracker.Repo.Model.Song

  @type t :: %__MODULE__{
          name: String.t()
        }

  schema "musical_kind" do
    field(:name, :string)
    belongs_to(:song, Song)
    timestamps(updated_at: false)
  end

  def changeset(musical_kind, attrs \\ %{}) do
    musical_kind
    |> cast(attrs, [:name])
    |> validate_required([:name])
  end

  def create_changeset(musical_kind, attrs \\ %{}) do
    musical_kind
    |> change()
    |> changeset(attrs)
  end
end
