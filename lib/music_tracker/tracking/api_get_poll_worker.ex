# Copyright 2022 Nicolas Jouanin <nicolas.jouanin@mailo.com>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# SPDX-License-Identifier: Apache-2.0

defmodule MusicTracker.Tracking.APIGetPollWorker do
  use Oban.Worker
  require Logger
  alias MusicTracker.Tracking.TrackingHelpers

  @headers [
    Pragma: "no-cache",
    Accept: "Application/json; Charset=utf-8",
    "Cache-Control": "no-cache"
  ]

  def perform(%{args: %{"args" => args, "context" => context}}) do
    with %{"url" => url} <- args,
         {:ok, channel} <- TrackingHelpers.get_channel(context) do
      Logger.info("[#{channel.name}] Starting 'poll' process.")

      case HTTPoison.get(url, @headers) do
        {:ok, %{status_code: status, body: body}} when status in 200..299 ->
          MusicTracker.Tracking.Process.continue_process({:ok, "poll", body}, context)

        {:error, %HTTPoison.Error{reason: reason}} ->
          MusicTracker.Tracking.Process.continue_process({:error_stop, "poll", reason}, context)
      end
    end
  end
end
