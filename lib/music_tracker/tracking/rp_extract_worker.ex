# Copyright 2022 Nicolas Jouanin <nicolas.jouanin@mailo.com>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# SPDX-License-Identifier: Apache-2.0
defmodule MusicTracker.Tracking.RPExtractWorker do
  alias MusicTracker.Tracking.TrackingHelpers
  use Oban.Worker

  @job "extract"

  def perform(%{args: %{"args" => _args, "context" => context}}) do
    with {:ok, poll_result} <- TrackingHelpers.get_poll_result(context),
         {:ok, %{"0" => song}, refresh} <- get_payload(poll_result) do
      # get payload without "moving" fields
      payload = Map.drop(song, ["rating"])
      hash = TrackingHelpers.hash_payload(payload)

      MusicTracker.Tracking.Process.continue_process(
        {:ok, @job, %{"payload" => song, "hash" => hash}},
        Map.put(context, "__schedule_in__", refresh)
      )
    else
      :error ->
        MusicTracker.Tracking.Process.continue_process(
          {:error_stop, @job, "Invalid context: Poll result not found"},
          context
        )

      {:error, :invalid_body} ->
        MusicTracker.Tracking.Process.continue_process(
          {:error_stop, @job, "Poll result decode error: invalid body"},
          context
        )
    end
  end

  defp get_payload(response_body) do
    case Jason.decode(response_body) do
      {:ok, %{"song" => song, "refresh" => refresh}} ->
        {:ok, song, refresh}

      {:error, _} ->
        {:error, :invalid_body}
    end
  end
end
