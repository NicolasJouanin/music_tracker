# Copyright 2022 Nicolas Jouanin <nicolas.jouanin@mailo.com>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# SPDX-License-Identifier: Apache-2.0
defmodule MusicTracker.Tracking.RadioFranceSaveWorker do
  alias MusicTracker.Tracking.TrackingHelpers
  alias MusicTracker.Repo.Songs
  use Oban.Worker
  require Logger

  @job "save"
  @analyzer_id "RadioFrance"

  def perform(%{args: %{"args" => _args, "context" => context}}) do
    with {:ok, %{"hash" => hash, "payload" => payload}} <-
           TrackingHelpers.get_extract_result(context),
         %{
           "media" => media,
           "now" => %{
             "cover" => cover,
             "song" => json_song,
             "firstLine" => first_line,
             "secondLine" => second_line
           }
         } <- payload,
         %{"id" => song_id} when not is_nil(song_id) <- json_song,
         {:ok, channel} <- TrackingHelpers.get_channel(context) do
      if Songs.exists_song_play_history_with_hash?(hash) do
        MusicTracker.Tracking.Process.continue_process(
          {:error_stop, @job, "Extracted payload already processed"},
          context
        )
      else
        title = Map.get(json_song, "title") || first_line

        interpreters =
          case Map.get(json_song, "interperters") do
            [] -> [second_line]
            nil -> [second_line]
            int -> int
          end

        album = Map.get(json_song, "release", %{}) |> Map.get("title", nil)
        label = Map.get(json_song, "release", %{}) |> Map.get("label", nil)

        # Look for an existing song with same UUID or title similarity
        song =
          case Songs.get_song_by_analyser_id_or_similarity(
                 {@analyzer_id, json_song["id"]},
                 title,
                 album,
                 interpreters
               ) do
            {:ok, nil} ->
              # Song not found, create a new one
              Logger.debug("Adding new song '#{title}'")

              {:ok, song} =
                %{
                  "title" => title |> clean_field() |> String.capitalize(),
                  "album" => album |> clean_field() |> String.capitalize(),
                  "label" => label |> clean_field() |> String.upcase(),
                  "year" => Map.get(json_song, "year")
                }
                |> Songs.insert_song()

              Songs.add_song_analyzer_id(song, @analyzer_id, Map.get(json_song, "id"))
              song

            {:ok, song} ->
              Logger.debug("Updating existing song")
              song |> Songs.update_song_fields(json_song)

            {:no_unique_similarity, query} ->
              Logger.warn(
                "One or more query match similarity criterias, title='#{title}', artists=#{json_song["interpreters"]}"
              )

              Logger.warn("Run to following query to learn more: #{query}")

              nil
          end

        # Add song artists
        Enum.each(interpreters, fn artist ->
          Songs.add_song_artist(song, String.trim(artist))
        end)

        # Add musical history
        start_time = Map.get(media, "startTime") |> DateTime.from_unix!()
        end_time = Map.get(media, "endTime") |> DateTime.from_unix!()

        Logger.debug("New '#{song.title}' play history on #{channel.name} at #{start_time}")

        Songs.add_song_play_history(song, hash, channel, start_time, end_time)

        # Add media
        Songs.add_song_media(song, "covers", %{"cover" => cover["src"]})

        Songs.broadcast_song_analyzed(channel, song)

        MusicTracker.Tracking.Process.continue_process({:ok, @job, song}, context)
      end
    else
      :error ->
        MusicTracker.Tracking.Process.continue_process(
          {:error_stop, @job, "Invalid context: Extract result not found"},
          context
        )

      _ ->
        MusicTracker.Tracking.Process.continue_process(
          {:error_stop, @job, "Invalid context: save job ended"},
          context
        )
    end
  end

  defp clean_field(field) do
    case field do
      nil -> ""
      other -> other
    end
    |> String.trim()
  end
end
