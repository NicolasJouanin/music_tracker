# Copyright 2022 Nicolas Jouanin <nicolas.jouanin@mailo.com>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# SPDX-License-Identifier: Apache-2.0

defmodule MusicTracker.RadioFrance.FIPBackfillWorker do
  use Oban.Worker, queue: :backfill
  require Logger
  alias MusicTracker.Repo.Channels

  @headers [
    Pragma: "no-cache",
    Accept: "Application/json; Charset=utf-8",
    "Cache-Control": "no-cache"
  ]

  @impl Oban.Worker
  def perform(%Oban.Job{
        args: %{
          "from" => from,
          "until" => until,
          "station_id" => station_id,
          "channel_name" => channel_name
        }
      }) do
    config = Application.get_env(:music_tracker, RadioFrance)
    url = String.replace(config[:fip_backfill_url], "__ID__", station_id)

    case Channels.get_by_name(channel_name) do
      nil ->
        Logger.warn("Unknown channel #{channel_name}, stopping.")
        :ok

      channel ->
        batch_size = config[:fip_backfill_batch_size]

        {:ok, from_dt, 0} = DateTime.from_iso8601(from)
        {:ok, until_dt, 0} = DateTime.from_iso8601(until)
        res = run_batch(channel, url, from_dt, until_dt, batch_size, [], 0)

        :ok
    end
  end

  defp run_batch(channel, url, from_dt, until_dt, batch_size, batch, count) do
    Logger.debug("Date cursors : from=#{inspect(from_dt)}, until=#{inspect(until_dt)}")

    case DateTime.compare(from_dt, until_dt) do
      # Stop recursion when from date becomes >= until_date
      :gt ->
        {:ok, count}

      :eq ->
        {:ok, count}

      :lt ->
        case batch do
          [elem | tail] ->
            # Pop elem and analyzes
            until_dt = Map.get(elem, "start_time") |> DateTime.from_unix!()
            hash = hash_payload(elem)

            play_log = %{
              payload: elem,
              hash: hash
            }

            case MusicTracker.Repo.PlayLogs.insert_play_log(channel, play_log) do
              {:ok, playlog} ->
                Logger.debug("[#{channel.name}] Backfilling new play log entry, hash=#{hash}")
                # Schedule analyze job
                Logger.debug(
                  "[#{channel.name}] Scheduling analyze for '#{Map.get(elem, "title")}', count=#{count}"
                )

                %{playlog_id: playlog.id, channel_id: channel.id}
                |> MusicTracker.RadioFrance.FIPAnalyzer.new(queue: :backfill_analyze)
                |> Oban.insert()

              {:error, message} ->
                Logger.debug("[#{channel.name}] Play log with hash=#{hash} already recorded")
            end

            # Then run batch on remaining items
            run_batch(channel, url, from_dt, until_dt, batch_size, tail, count + 1)

          [] ->
            # No item in current batch. Fetch new one
            Logger.debug("Polling new history batch")
            {:ok, batch} = poll_history(url, until_dt, batch_size)
            run_batch(channel, url, from_dt, until_dt, batch_size, batch, count)
        end
    end
  end

  @spec poll_history(
          history_url :: String.t(),
          after_datetime :: DateTime.t(),
          batch_size :: integer()
        ) :: {:ok, map()} | {:error, String.t()}
  def poll_history(history_url, after_datetime, batch_size) do
    after_ts = after_datetime |> DateTime.to_unix() |> Integer.to_string() |> Base.encode64()

    url =
      history_url
      |> String.replace("__BATCH_SIZE__", Integer.to_string(batch_size))
      |> String.replace("__AFTER_TIMESTAMP__", after_ts)

    with {:ok, %{status_code: status, body: body}} when status in 200..299 <-
           HTTPoison.get(url, @headers),
         {:ok, history} <- Jason.decode(body),
         %{"data" => %{"timelineCursor" => %{"edges" => edges}}} <- history do
      nodes =
        Enum.map(edges, fn edge -> Map.get(edge, "node") end)
        |> Enum.sort_by(&Map.get(&1, "start_time"), :desc)

      {:ok, nodes}
    else
      {:error, reason} ->
        {:error, "Poll failed. Reason: #{reason}."}
    end
  end

  @spec hash_payload(payload :: map()) :: String.t()
  defp hash_payload(payload) do
    hash_source = payload |> Jason.encode!()
    :crypto.hash(:sha256, hash_source) |> Base.encode64()
  end
end
