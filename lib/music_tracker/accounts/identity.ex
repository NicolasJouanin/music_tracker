defmodule MusicTracker.Accounts.Identity do
  use Ecto.Schema
  import Ecto.Changeset

  alias MusicTracker.Accounts.{Identity, User}

  @google "google"

  @primary_key {:id, :binary_id, autogenerate: true}
  @foreign_key_type :binary_id
  schema "identities" do
    field :provider, :string
    field :provider_token, :string
    field :provider_email, :string
    field :provider_id, :string
    field :provider_meta, :map

    belongs_to :user, User

    timestamps()
  end

  def registration_changeset(:google, auth) do
    %{credentials: credentials, info: info} = auth

    params =
      %{
        "provider" => @google,
        "provider_token" => credentials.token,
        "provider_email" => info.email,
        "provider_id" => auth.uid,
        "provider_meta" => %{info: Map.from_struct(info)}
      }
      |> changeset()
  end

  def changeset(params) do
    %Identity{}
    |> cast(params, [:provider, :provider_token, :provider_email, :provider_id, :provider_meta])
    |> validate_required([:provider_token, :provider_email, :provider, :provider_id])
  end

  def update_token_changeset(:google, identity, auth) do
    %{credentials: credentials, info: info} = auth

    identity
    |> change()
    |> put_change(:provider_token, credentials.token)
  end
end
