defmodule MusicTracker.Accounts.User do
  use Ecto.Schema
  import Ecto.Changeset
  import MusicTracker.Gettext

  alias MusicTracker.Accounts.{User, Identity}

  @primary_key {:id, :binary_id, autogenerate: true}
  @foreign_key_type :binary_id
  schema "users" do
    field :email, :string
    field :username, :string
    field :confirmed_at, :naive_datetime
    field :role, :string, default: "user"
    field :avatar_url, :string
    field :name, :string
    has_many :identities, Identity

    timestamps()
  end

  def registration_changeset(:google, auth, identity_changeset) do
    %{credentials: _credentials, info: info} = auth

    %{
      "email" => info.email,
      "username" => info.email,
      "avatar_url" => info.image,
      "name" => info.name || info.first_name <> " " <> info.last_name
    }
    |> changeset(identity_changeset)
  end

  def changeset(params, identity_changeset) do
    %User{}
    |> cast(params, [:email, :avatar_url, :username, :name])
    |> validate_email()
    |> validate_username()
    |> put_assoc(:identities, [identity_changeset])
  end

  defp validate_email(changeset) do
    changeset
    |> validate_required([:email])
    |> validate_format(:email, ~r/^[^\s]+@[^\s]+$/, message: "must have the @ sign and no spaces")
    |> validate_length(:email, max: 160)
    |> unsafe_validate_unique(:email, MusicTracker.Repo,
      message: gettext("Email address already used by some user")
    )
    |> unique_constraint(:email)
  end

  defp validate_username(changeset) do
    changeset
    |> validate_required([:username])
    |> unsafe_validate_unique(:username, MusicTracker.Repo)
    |> unique_constraint(:username)
  end

  def confirmation_changeset(user, attrs \\ %{}) do
    user
    |> cast(attrs, [:email])
    |> validate_required([:email])
  end

  @doc """
  A user changeset for changing the email.

  It requires the email to change otherwise an error is added.
  """
  def email_changeset(user, attrs) do
    user
    |> cast(attrs, [:email])
    |> validate_email()
    |> case do
      %{changes: %{email: _}} = changeset -> changeset
      %{} = changeset -> add_error(changeset, :email, "did not change")
    end
  end

  @doc """
  Confirms the account by setting `confirmed_at`.
  """
  def confirm_changeset(user) do
    now = NaiveDateTime.utc_now() |> NaiveDateTime.truncate(:second)
    change(user, confirmed_at: now)
  end
end
