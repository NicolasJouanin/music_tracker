# Copyright 2022 Nicolas Jouanin <nicolas.jouanin@mailo.com>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
# SPDX-License-Identifier: Apache-2.0

defmodule MusicTracker.TrackerServer do
  use GenServer
  require Logger
  alias MusicTracker.Repo.Channels
  alias MusicTracker.Repo.Model.PlayLog
  alias MusicTracker.Repo.Model.Channel

  @max_next_poll 10
  @max_next_poll 60

  @type payload :: map()
  @type next_poll :: integer()
  @type error_message :: String.t()
  @type poll_params :: map()
  @type poll_result :: {:ok, payload(), next_poll()} | {:error, error_message()}

  @callback poll(Channel.t(), poll_params()) :: poll_result()
  @callback cast_analyze(Channel.t(), PlayLog.t()) :: :ok

  def start_link(%{channel_name: channel_name} = args) do
    GenServer.start_link(__MODULE__, args, name: {:global, channel_name})
  end

  @impl true
  def init(%{channel_name: channel_name, impl: impl, poll_params: poll_params}) do
    case Channels.get_by_name(channel_name) do
      nil ->
        Logger.warn("TrackerServer started for unknown channel #{channel_name}, ignoring.")
        :ignore

      channel ->
        state = %{
          poll_params: poll_params,
          impl: impl,
          channel: channel,
          last_hash: nil
        }

        # TODO cast analyze for un-analyzed playlog which may exists in DB
        Process.send(self(), :poll, [:noconnect])
        {:ok, state}
    end
  end

  @impl true
  def handle_info(:poll, state) do
    %{poll_params: poll_params, impl: impl, last_hash: last_hash, channel: channel} = state
    Logger.debug("[#{channel.name}] Polling channel with implementation #{impl}")

    case impl.poll(channel, poll_params) do
      {:ok, payload, next_poll} ->
        hash = hash_payload(payload)

        if hash != last_hash do
          # Response has changed, save new log
          play_log = %{
            payload: payload,
            hash: hash
          }

          case MusicTracker.Repo.PlayLogs.insert_play_log(channel, play_log) do
            {:ok, playlog} ->
              Logger.debug("[#{channel.name}] Polled new play log entry, hash=#{hash}")
              # Schedule analyze job
              impl.cast_analyze(channel, playlog)

            {:error, message} ->
              Logger.debug("#{inspect(message)}")
              Logger.debug("[#{channel.name}] Play log with hash=#{hash} already recorded")
          end
        else
          # Response has not changed, it is already logged
          Logger.debug("[#{channel.name}] Play log with hash=#{hash} already recorded")
        end

        # Delay 10 < x < @max_next_poll
        delay = max(min(next_poll, @max_next_poll), 10)
        Process.send_after(self(), :poll, delay * 1_000)
        Logger.debug("[#{channel.name}] Next poll in #{delay} seconds")

        state = Map.put(state, last_hash, hash)
        {:noreply, state}

      {:error, reason} ->
        Logger.warn(
          "#{impl}.poll() failed. Reason: #{inspect(reason)}. Retrying in #{@max_next_poll} seconds"
        )

        Process.send_after(self(), :poll, @max_next_poll * 1_000)
        {:noreply, state}
    end
  end

  @spec hash_payload(payload :: map()) :: String.t()
  defp hash_payload(payload) do
    hash_source = payload |> Jason.encode!()
    :crypto.hash(:sha256, hash_source) |> Base.encode64()
  end
end
