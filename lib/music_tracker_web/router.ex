defmodule MusicTrackerWeb.Router do
  use MusicTrackerWeb, :router

  import MusicTrackerWeb.UserAuth

  pipeline :browser do
    plug :accepts, ["html"]
    plug :fetch_session
    plug :fetch_live_flash
    plug :put_root_layout, {MusicTrackerWeb.LayoutView, :root}
    plug :protect_from_forgery
    plug :put_secure_browser_headers
    plug :fetch_current_user

    plug(Cldr.Plug.SetLocale,
      apps: [:cldr],
      cldr: MusicTracker.Cldr
    )

    plug(:put_locale_into_session)
  end

  def put_locale_into_session(conn, _opts) do
    put_session(conn, "locale", Cldr.LanguageTag.to_string(conn.private.cldr_locale))
  end

  pipeline :api do
    plug :accepts, ["json"]
  end

  # Other scopes may use custom stacks.
  # scope "/api", MusicTrackerWeb do
  #   pipe_through :api
  # end

  # Enables LiveDashboard only for development
  #
  # If you want to use the LiveDashboard in production, you should put
  # it behind authentication and allow only admins to access it.
  # If your application does not have an admins-only section yet,
  # you can use Plug.BasicAuth to set up some basic authentication
  # as long as you are also using SSL (which you should anyway).
  if Mix.env() in [:dev, :test] do
    import Phoenix.LiveDashboard.Router

    scope "/" do
      pipe_through :browser

      live_dashboard "/phoenix_dashboard", metrics: MusicTrackerWeb.Telemetry
    end
  end

  # Enables the Swoosh mailbox preview in development.
  #
  # Note that preview only shows emails that were sent by the same
  # node running the Phoenix server.
  if Mix.env() == :dev do
    scope "/dev" do
      pipe_through :browser

      forward "/mailbox", Plug.Swoosh.MailboxPreview
    end
  end

  scope "/", MusicTrackerWeb do
    pipe_through [:browser]

    get "/", RedirectController, :redirect_authenticated

    live_session :default,
      on_mount: [
        {MusicTrackerWeb.UserAuth, :current_user},
        {MusicTrackerWeb.UserAuth, :set_locale},
        MusicTrackerWeb.Navigation
      ] do
      live "/dashboard", DashboardLive
      live "/users/sign_in", SignInLive
    end

    live_session :app,
      on_mount: [
        {MusicTrackerWeb.UserAuth, :ensure_authenticated},
        {MusicTrackerWeb.UserAuth, :current_user},
        {MusicTrackerWeb.UserAuth, :set_locale},
        MusicTrackerWeb.Navigation
      ] do
      live "/profile/:username", AppLive, :profile
      live "/settings", AppLive, :settings
    end

    delete "/users/log_out", OAuthCallbackController, :sign_out
  end

  scope "/auth", MusicTrackerWeb do
    pipe_through :browser

    get "/:provider", OAuthCallbackController, :request
    get "/:provider/callback", OAuthCallbackController, :callback
  end
end
