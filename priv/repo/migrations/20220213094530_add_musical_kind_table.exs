defmodule MusicTracker.Repo.Migrations.AddMusicalKindTable do
  use Ecto.Migration

  def change do
    create table(:musical_kind) do
      add :song_id, references(:song, on_delete: :delete_all)
      add :name, :string
      timestamps(updated_at: false)
    end

    create index(:musical_kind, ["name gin_trgm_ops"],
             name: :musical_kind_name_trigram_index,
             using: :gin
           )
  end
end
