defmodule MusicTracker.Repo.Migrations.AddChannelTable do
  use Ecto.Migration

  def change do
    create table(:channel) do
      add :name, :string, null: false
      add :homepage, :string, null: false
      add :logo_url, :string
      add :tracking_activated, :boolean, default: false
      add :poll_worker, :string
      add :poll_args, :map, default: %{}
      add :extract_worker, :string
      add :extract_args, :map, default: %{}
      add :save_worker, :string
      add :save_args, :map, default: %{}
      timestamps()
    end

    create unique_index(:channel, [:name])
  end
end
