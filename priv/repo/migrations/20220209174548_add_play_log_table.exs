defmodule MusicTracker.Repo.Migrations.AddPlayLogTable do
  use Ecto.Migration

  def change do
    create table(:play_log) do
      add :payload, :map
      add :hash, :string
      add :analyzed_at, :utc_datetime_usec
      add :channel_id, references(:channel, on_delete: :delete_all), null: false
      timestamps(updated_at: false)
    end

    create unique_index(:play_log, [:hash])
  end
end
