defmodule MusicTracker.Repo.Migrations.AddSongAnalyzerIdTable do
  use Ecto.Migration

  def change do
    create table(:song_analyer_id) do
      add(:song_id, references(:song, on_delete: :delete_all))
      add(:analyzer_name, :string)
      add(:analyzer_song_uuid, :string)
      timestamps(updated_at: false)
    end
  end
end
