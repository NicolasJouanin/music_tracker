defmodule MusicTracker.Repo.Migrations.AddSongArtistTable do
  use Ecto.Migration

  def change do
    create table(:song_artist) do
      add :song_id, references(:song, on_delete: :delete_all)
      add :name, :string
      timestamps(updated_at: false)
    end

    create index(:song_artist, ["name gin_trgm_ops"],
             name: :song_artist_name_trigram_index,
             using: :gin
           )
  end
end
