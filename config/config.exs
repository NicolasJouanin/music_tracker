# This file is responsible for configuring your application
# and its dependencies with the aid of the Config module.
#
# This configuration file is loaded before any dependency and
# is restricted to this project.

# General application configuration
import Config

config :music_tracker,
  ecto_repos: [MusicTracker.Repo],
  generators: [binary_id: true]

config :music_tracker, MusicTracker.Repo,
  migration_primary_key: [name: :id, type: :binary_id],
  migration_timestamps: [type: :utc_datetime_usec]

# Configures the endpoint
config :music_tracker, MusicTrackerWeb.Endpoint,
  url: [host: "localhost"],
  render_errors: [view: MusicTrackerWeb.ErrorView, accepts: ~w(html json), layout: false],
  pubsub_server: MusicTracker.PubSub,
  live_view: [signing_salt: "daac9Ao6"]

# Configures the mailer
#
# By default it uses the "Local" adapter which stores the emails
# locally. You can see the emails in your browser, at "/dev/mailbox".
#
# For production it's recommended to configure a different adapter
# at the `config/runtime.exs`.
config :music_tracker, MusicTracker.Mailer, adapter: Swoosh.Adapters.Local

# Swoosh API client is needed for adapters other than SMTP.
config :swoosh, :api_client, false

# Configure esbuild (the version is required)
config :esbuild,
  version: "0.14.0",
  default: [
    args:
      ~w(js/app.js --bundle --target=es2017 --outdir=../priv/static/assets --external:/fonts/* --external:/images/*),
    cd: Path.expand("../assets", __DIR__),
    env: %{"NODE_PATH" => Path.expand("../deps", __DIR__)}
  ]

config :tailwind,
  version: "3.0.12",
  default: [
    args: ~w(
      --config=tailwind.config.js
      --input=css/app.css
      --output=../priv/static/assets/app.css
    ),
    cd: Path.expand("../assets", __DIR__)
  ]

# Configures Elixir's Logger
config :logger, :console,
  format: "$time $metadata[$level] $message\n",
  metadata: [:request_id]

# Use Jason for JSON parsing in Phoenix
config :phoenix, :json_library, Jason

config :elixir, :time_zone_database, Tzdata.TimeZoneDatabase

config :music_tracker, Oban,
  repo: MusicTracker.Repo,
  plugins: [Oban.Plugins.Pruner],
  queues: [default: 5, concurrent_analyze: 5, backfill: 1, backfill_analyze: 1]

config :music_tracker, RadioFrance,
  fip_tracker_url:
    ~s|https://www.fip.fr/latest/api/graphql?operationName=Now&variables={"stationId"%3A__ID__}&extensions={"persistedQuery"%3A{"version"%3A1%2C"sha256Hash"%3A"95ed3dd1212114e94d459439bd60390b4d6f9e37b38baf8fd9653328ceb3b86b"}}|,
  fip_backfill_url:
    ~s|https://www.fip.fr/latest/api/graphql?operationName=History&variables={%22first%22%3A__BATCH_SIZE__%2C%22after%22%3A%22__AFTER_TIMESTAMP__%3D%3D%22%2C%22stationId%22%3A__ID__}&extensions={%22persistedQuery%22%3A{%22version%22%3A1%2C%22sha256Hash%22%3A%227530970d2ffdf4b2cb739123898944594f32dcfa4060c9b49e9e41d54d9f7857%22}}|,
  fip_backfill_batch_size: 10

config :music_tracker, RadioParadise,
  tracker_url: ~s|https://api.radioparadise.com/api/nowplaying_list?chan=__ID__|

config :ueberauth, Ueberauth,
  providers: [
    google: {Ueberauth.Strategy.Google, [default_scope: "email profile"]}
  ]

config :unsplash,
  application_redirect_uri: "urn:ietf:wg:oauth:2.0:oob"

# Import environment specific config. This must remain at the bottom
# of this file so it overrides the configuration defined above.
import_config "#{config_env()}.exs"
